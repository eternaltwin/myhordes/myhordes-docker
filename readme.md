# MyHordes Docker Wrapper

This wrapper can be used to run MyHordes with docker containers.

**Important:** This setup is designed for development purposes. Therefore it is optimized towards being easy
to debug and **not** for stability, speed or security. Never use this setup to run MyHordes on a public server!

## Creating the environment file
Within the MyHordes Docker folder, you will find a file called `.env.example`. Create a copy of this file named `.env`
before continuing. If you want to use the Crowdin translation service, edit your `.env` file and add the project ID and
your personal Crowdin API key.

## Setting up the environment

### Windows

Using this environment on Windows requires at least Windows 10 2004. Windows 11 is supported, as well.

**Make sure that Hardware Virtualisation is enabled within your BIOS/UEFI, otherwise WSL2 and Docker won't work (Intel VT-d or AMD-V)**

1. Enable the Windows Subsystem for Linux (WSL) and the Virtual Machine platform. Run the following code in a PowerShell terminal with elevated rights.
~~~powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
~~~
2. Reboot your PC.
3. Set the default WSL version to v2 by running the following code in a PowerShell terminal.
~~~powershell
wsl --set-default-version 2
~~~
4. Once set, install the Kernel Update [here](https://docs.microsoft.com/windows/wsl/wsl2-kernel)
5. Install Debian from the Windows Store: [here](ms-windows-store://pdp/?ProductId=9MSVKQC78PK6)
6. Run Debian and follow the first-launch setup steps.
7. Download and install the latest version of Docker Desktop: https://docs.docker.com/desktop/windows/wsl/
   1. Check _Use WSL2 instead of Hyper-V (Recommended)_
8. Open Docker Desktop and verify/adjust the following settings:
   1. [General] _Use the WSL2 based engine_ `ON`
   2. [General] _Use Docker Compose V2_ `ON`
   3. [Resources / WSL Integration] _Enable integration with additional distros_ `Debian: On`
9. Within Debian, install `git`
~~~bash
sudo apt update
sudo apt install git
~~~
10. Within Debian, check out this repository into a location of your choice and continue with **Setting up the repository**.

### Ubuntu / Debian
TODO

### Mac OS

1. Install Docker Desktop. Be sure to select the right installer if you have an Intel Mac or an Apple Silicon Mac (M1 or M2 chip) from [Docker's Website](https://www.docker.com/products/docker-desktop/)
2. Run it and allow it to install its features (such as networking) by entering your password
3. Once everything is set up, you should see a green docker background on the bottom left of the app.
4. Ensure that `git` and `curl` are available in your command line.
5. Clone this repository into the location of your choice
6. If you are using Mac computers with Apple silicon you might need to add `platform: linux/x86_64` to the **mariadb** of the *docker-compose.yml*
7. Continue with **Setting up the repository**

## Setting up the repository

1. Install cURL (if not already installed)
~~~bash
sudo apt install curl
~~~

2. Clone repository and initialize git submodules: 
~~~bash
git clone git@gitlab.com:eternaltwin/myhordes/myhordes-docker.git
cd myhordes-docker
git submodule update --init --recursive --remote
~~~
Alternatively, if you do not have a GitLab account with an associated SSH key, replace the first line with
~~~bash
git clone https://gitlab.com/eternaltwin/myhordes/myhordes-docker.git
~~~
3. Inside the newly created `./myhordes-docker` : Copy the env configuration file and edit it to your preference
~~~bash
cp .env.example .env
~~~
4. Start the containers
~~~bash
docker compose up -d
~~~
5. Install composer and all MyHordes project dependencies locally
~~~bash
tools/composer install
tools/yarn install
# If yarn install exits silently at the fetch step, try running the command 'tools/yarn install --json'
~~~
6. Compile the assets
~~~bash
tools/yarn encore dev
~~~

## Developing

### Distribution Packages (such as MyHordes Prime)

MyHordes can be extended by custom distribution packages, adding or modifying constructions, items, game rules and much more. One of these distribution packages is the MyHordes Prime Closes Source Package (`myhordes/prime-csc`), which is used on the main MyHordes server (https://myhordes.eu).

**If you do not have access to this package, or want to use your own package**, you need to update the composer lockfile:
```bash
tools/composer update myhordes/prime --no-scripts
```

**If you have access to this package**, you can enable it using the following command after starting the docker containers:
```bash
(cd myhordes && ln -s .composer.dist.json composer.dist.json) && tools/composer update myhordes/prime --no-scripts
```

**If you wish to use your own distribution package**, create a file named `composer.dist.json` within the `myhordes` folder:

!! Unless you're using the prime package, please make sure you do not commit your composer or symfony lock file!

```json
{
  "require": {
    "my-custom/dist-package": "*"
  },
  "_ add repository to your dist package below if needed": true,
  "repositories": [ ]
}
```

### Operating the MyHordes server

#### Starting the server

Run the servers in the current terminal:
~~~bash
docker compose up
~~~
Run the servers detached the current terminal:
~~~bash
docker compose up -d
~~~

This will run the entire MyHordes and Eternaltwin stack locally. Sometimes, you may only need a certain subset of this. You can restrict what services are run by specifying one or more profile options like this:

~~~bash
docker compose --profile <profile> --profile <another profile> up -d
~~~

#### Available profiles
| Profile    | Description                                                                                                                                                                                                                                                              |
|------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `all`      | Runs the entire stack (default)                                                                                                                                                                                                                                          |
| `cli`      | Runs the minimal amount of services possible for a MyHordes command line interface (database and application server)                                                                                                                                                     |
| `core`     | Runs the minimal amount of services possible for MyHordes (database, application server, webserver, router). Note that some functions like Discord notifications or the Nightly Attack will not work without the `async` profile, so you may want to enable that as well |
| `async`    | Runs cron and worker servers (must be used together with `core` or `cli` to work)                                                                                                                                                                                        |
| `dev`      | Runs dev helpers (phpMyAdmin and MailHog)                                                                                                                                                                                                                                |
| `myhordes` | Runs the full MyHordes stack (application server, cron and worker, database, webserver, router)                                                                                                                                                                          |
| `etwin`    | Runs the full Eternaltwin stack (application server, database and router)                                                                                                                                                                                                |
| `data`     | Runs only database servers without their associated application or web servers.                                                                                                                                                                                          |


#### Stopping the server
If the server is running in the current terminal, you can stop it simply by cancelling the command (`CTRL + C`).
Otherwise, use the following command to stop a detached server
~~~bash
docker compose down
~~~
To stop the server and delete the database at the same time, run
~~~bash
docker compose down --volumes
~~~

### Initialize the installation

Create the database:
```bash
tools/console app:migrate -i
```

### Domains and Services
| Service              | Domain                       | Port |
|----------------------|------------------------------|------|
| MyHordes Game Server | `myhordes.localhost`         | 80   |
| PHPMyAdmin           | `pma.myhordes.localhost`     | 80   |
| Mailhog              | `mailhog.myhordes.localhost` | 80   |
| Traeffic Dashboard   | `localhost`                  | 8080 |
| MariaDB              | `localhost`                  | 3306 |

### Tools

| Tool             | Description                                                                                                                                                                                                                           |
|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `tools/composer` | Runs the `composer` command within the PHP container. All input arguments are propagated into the container.                                                                                                                          |
| `tools/console`  | Runs Symfony's `bin/console` command within the PHP container. All input arguments are propagated into the container.                                                                                                                 |
| `tools/yarn`     | Runs the `yarn` command within the PHP container. All input arguments are propagated.                                                                                                                                                 |
| `tools/npm`      | Runs the `npm` command within the PHP container. All input arguments are propagated.                                                                                                                                                  |
| `tools/php`      | Shortcut to executing code in the PHP container.                                                                                                                                                                                      |
| `tools/crowdin`  | Runs the `crowdin` command within the PHP container. Use `tools/crowdin up` or `tools/crowdin down` as a shortcut to upload or download translations.                                                                                 |
| `tools/snapshot` | Database backup tool. Use `tools/snapshot create <name>` to create a named backup, `tools/snapshot restore <name>` to restore one and `tools/snapshot delete <name>` to delete a backup. `<name>` is optional and defaults to "snap". |